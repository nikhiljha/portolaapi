const express = require('express')
const app = express()
var events = require('./events.json')

app.get('/api/time', function (req, res) {
	// TODO: Calculate the time till the next class.
	// TODO: Make this not bad. (It's slow right now, for obvious reasons.)

	// Parse and filter the events.
	var now = new Date();
	events = events.filter(event => new Date(event.end) > now);
	ret = ""

	console.log(events[0])

	// Calculate the string.
	if (now < events[0].date) {
		// Event happening right now.
		ret = (new Date(events[0].end) - now) / 1000 / 60
		ret = Math.floor(ret);
		ret = ret + "end"
	} else {
		// Event not happening right now.
		var ret = (new Date(events[0].date) - now) / 1000 / 60
		ret = Math.floor(ret);
                ret = ret + "sta"
	}
	res.send('{ "text": "' + ret + '" }');
})

app.listen(3000, () => console.log('Portola API listening on port 3000.'))
